# -*- coding: utf-8 -*-
import sys
import re
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import chromedriver_binary
import csv
import os
from datetime import datetime

url = 'https://moneyforward.com/accounts/show_manual/' + os.environ['ACCOUNT']
user = os.environ['USER']
password = os.environ['PASSWORD']

def get_select(content):
    #judge content
    if ('JAYA GROCER' in content or
        'B.I.G SUPERMARKET' in content or
        'VILLAGE GROCER' in content or
        'AEON BIG' in content or
        'AEON SMKT' in content or
        'BIG-PUBLIKA' in content or
        'JAPAN GROCER' in content or
        'KIARA SUPERMARKET' in content or
        'SHOJIKIYA' in content or
        'TSUKIJI KUALA LUMPUR' in content or
        'HAPPYFRESH' in content or
        'TESCO MY' in content) :
        return '食費','食料品'

    elif ('GRAB ECOM' in content or
        'GRAB -EC' in content or
        'FOODPANDA' in content or
        'LAVENDER ' in content or
        'FAMILYMART' in content or
        'FAVE MY KUALA LUMPUR' in content or
        'MYNEWS.COM' in content):
        return '食費','外食'

    elif ('AEON WELLNESS' in content or
        'CARING ' in content):
        return '日用品','ドラッグストア'

    elif ('HIBARI CLINIC' in content):
        return '健康・医療','医療費'

    elif ('UNIQLO' in content or
        'H & M' in content):
        return '衣服・美容','衣服'

    elif ('PICHOUNE' in content):
        return '衣服・美容','美容院・理髪'

    elif ('FPXPAY GHL EPAYMENTS' in content):
        return '趣味・娯楽','映画・音楽・ゲーム'

    elif ('ATM WITHDRAWAL' in content):
        return '現金・カード','ATM引き出し'

    elif ('GRABPAY' in content):
        return '現金・カード','電子マネー'

    elif ('PAY TO GAS MALAYSIA' in content):
        return '水道・光熱費','ガス・灯油代'

    elif ('JOMPAY|5454' in content):
        return '水道・光熱費','電気代'

    elif ('Aman Rental Fee' in content):
        return '住宅','家賃・地代'

    elif ('TT DOTCOM SDN' in content):
        return '通信費','インターネット'

    elif ('SHELL-' in content):
        return '自動車','ガソリン'

    return None,None


if len(sys.argv) != 2:
    print("No input_file!")
    print("usage: python mf_import_csv.py data_file.csv")
    sys.exit()
input_file = str(sys.argv[1])

try:
    print("Start :" + input_file)

    #Chromeブラウザを立ち上げる
    driver = webdriver.Chrome()
    driver.implicitly_wait(10)   #wait

    #マネーフォワードの銀行ページに遷移
    driver.get(url)

    #アカウント入力
    elem = driver.find_element(By.ID, "mfid_user[email]")
    elem.clear()
    elem.send_keys(user)
    elem.send_keys(Keys.ENTER)

    driver.implicitly_wait(3)   #wait

    #パスワード入力
    elem = driver.find_element(By.ID, "mfid_user[password]")
    elem.clear()
    elem.send_keys(password)
    elem.send_keys(Keys.ENTER)

    driver.implicitly_wait(3)   #wait

    #open data file
    f = open(input_file, mode='r', encoding='utf_8_sig')

    reader = csv.reader(f)
    n = 0
    for row in reader:
        n+=1

        #skip comment
        if '#' == row[0][0]:
            print("[" + str(n) + "] Skip comment line!")
            continue

        #input button click
        elem = driver.find_element(By.CLASS_NAME, "cf-new-btn")
        elem.click()
        driver.implicitly_wait(7)   #wait

        plus_minus_flg = None
        if row[2] and int(row[2].replace(',', '')) > 0:
            print("[" + str(n) + "] " + "minus! :")
            print(row)
            amount = int(row[2].replace(',', ''))
            plus_minus_flg = 'm'

            #click minus

        elif row[3] and int(row[3].replace(',', '')) > 0:
            print("[" + str(n) + "] " + "Plus! :")
            print(row)
            amount = int(row[3].replace(',', ''))
            plus_minus_flg = 'p'

            #click Plus
            elem = driver.find_element(By.CLASS_NAME, "plus-payment")
            elem.click()
        else:
            print ("Error row num = " + str(n) + "\n")

        driver.implicitly_wait(5)   #wait

        #string to date
        dt = row[0].split(" ")[0]
        dt = datetime.strptime(dt, '%Y-%m-%d')
        str_dt = dt.strftime('%Y/%m/%d')
        elem = WebDriverWait(driver, 8).until(
            EC.visibility_of_element_located((By.ID, "updated-at"))
        )
        elem.clear()
        elem.send_keys(str_dt)

        #date module close
        if 'p' == plus_minus_flg:
            #click Plus
            elem = driver.find_element(By.CLASS_NAME, "plus-payment")
            elem.click()

        elif 'm' == plus_minus_flg:
            #click Minus
            elem = driver.find_element(By.CLASS_NAME, "minus-payment")
            elem.click()

        else:
            print ("Error row num = " + str(n) + "\n")

        #amount
        elem = driver.find_element(By.ID, "appendedPrependedInput")
        elem.clear()
        elem.send_keys(amount)

        #content with 51 charactor
        content1 = re.sub(r"\s+", " ", row[1])
        content2 = re.sub('\|{2,}', "|", content1)
        content50 = content2[0:50]
        print("[" + str(n) + "] content sub:" + content2)
        print("[" + str(n) + "] content 50:" + content50)

        #select kind
        large, middle = get_select(content2)
        if 'm' == plus_minus_flg and large != None:
            #test
            print("[" + str(n) + "] large:" + str(large))
            print("[" + str(n) + "] middle:" + str(middle))

            elem = WebDriverWait(driver, 5).until(
                EC.visibility_of_element_located((By.ID, "js-large-category-selected"))
            )
            elem = driver.find_element(By.ID, "js-large-category-selected")
            elem.click()

            elem = driver.find_element(By.LINK_TEXT, large)
            elem.click()

            elem = WebDriverWait(driver, 5).until(
                EC.visibility_of_element_located((By.ID, "js-middle-category-selected"))
            )
            elem = driver.find_element(By.ID, "js-middle-category-selected")
            elem.click()

            elem = driver.find_element(By.LINK_TEXT, middle)
            elem.click()

        elem = driver.find_element(By.ID, "js-content-field")
        elem.clear()
        elem.send_keys(content50)

        #click
        elem = driver.find_element(By.ID, "submit-button")
        elem.click()

        driver.implicitly_wait(8)   #wait
        driver.get(url)
        driver.implicitly_wait(3)   #wait

        #test
        #if n == 5:
        #    sys.exit()

    f.close()

finally:
    print("End :" + input_file)
    #driver.quit()
